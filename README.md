## Hi there 👋

I am engaged in the development of custom sites, plugins for Minecraft, projects in Java or Kotlin and much more.

---

### 💻 Stack:
* HTML, CSS/SCSS, JS;
* PHP, Laravel, Yii2;
* SQL, MySQL;
* Java, Kotlin;
* C++, C#, QT;
* Linux administration.

---

### 📱 Social networks:
* [TG](https://t.me/fayence/)
* [TG channel](https://t.me/dadowldev)
* [Twitter](https://twitter.com/fayence_)
* [VK](https://vk.com/fayence)
